import { render, screen } from '@testing-library/react';
import App from './App';

describe('App component', () => {
  test('renders header with correct text', () => {
    render(<App />);
    const headerElement = screen.getByText('TMDB Code Challenge');
    expect(headerElement).toBeTruthy();
  });
});
